use futures::channel::{mpsc, oneshot};
use futures::prelude::*;
use futures::stream::StreamExt;
use std::borrow;
use std::cell::RefCell;
use std::path::PathBuf;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::time::Duration;

use clap::Parser;

use glib::object::{Cast, ObjectExt};
use glib::{clone, g_critical, GString, MainLoop};

use gupnp::prelude::*;
use gupnp::*;

use gupnp_av::prelude::{ProtocolInfoExt, *};
use gupnp_av::*;

use indicatif::ProgressBar;

///  Upload files to UPnP Media Server
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Search timeout (in seconds)
    #[arg(short, long, default_value_t = 0)]
    search_timeout: u64,

    /// Destination container Id
    #[arg(short, long)]
    container_id: Option<String>,

    /// Title for item. If not provided will be derived from filename
    #[arg(short, long)]
    title: Option<String>,

    /// Network interface to search MediaServer on
    #[arg(short, long)]
    interface: Option<String>,

    /// UDN of the device to upload to
    #[arg(short, long)]
    udn: String,

    #[clap(required = true)]
    files: Vec<PathBuf>,
}

async fn upload(args: &Args) {
    let context = gupnp::Context::new_full(
        args.interface.as_ref().map(String::as_str),
        None::<&gio::InetAddress>,
        0,
        gssdp::UDAVersion::Version10,
    );

    let ctx = match context {
        Err(err) => {
            println!("Failed to create context: {}", err.message());
            std::process::exit(6);
        }
        Ok(v) => v.unwrap_or_else(|| panic!("Null context without error. Must not happen")),
    };

    println!(
        "UPnP context created for interface {} ({})",
        gupnp::prelude::ClientExt::interface(&ctx).unwrap(),
        ctx.host_ip().unwrap()
    );

    let cp = gupnp::ControlPoint::new(&ctx, &args.udn);
    cp.set_active(true);

    let service = {
        let (sender, mut receiver) = mpsc::channel(1);
        let s = Arc::new(Mutex::new(sender));
        let proxy_available_id = cp.connect_device_proxy_available(move |_, device| {
            const CDS: &str = "urn:schemas-upnp-org:service:ContentDirectory";
            let _ = s.lock().unwrap().try_send(device.service(CDS));
        });

        let service = match glib::future_with_timeout(
            Duration::from_secs(args.search_timeout),
            receiver.next(),
        )
        .await
        {
            Err(e) => {
                println!("Timeout while waiting for device {}: {}", args.udn, e);
                None
            }
            Ok(result) => result.unwrap(),
        };
        cp.disconnect(proxy_available_id);

        service
    };

    if service.is_none() {
        println!("Could not find any service proxy...");
        return;
    }

    let cds = service.unwrap().downcast::<gupnp::ServiceProxy>().unwrap();

    // Don't care about the introspection, just make sure it is available for the
    // iterator in the proxy calls
    let _ = cds.introspect_future().await;

    let mut container = args.container_id.clone();
    if container.is_none() {
        let action = ServiceProxyAction::new_plain("Browse")
            .add_argument("ObjectID", &glib::Value::from(0))
            .unwrap()
            .add_argument("BrowseFlag", &glib::Value::from("BrowseDirectChildren"))
            .unwrap()
            .add_argument("Filter", &glib::Value::from("*"))
            .unwrap()
            .add_argument("StartingIndex", &glib::Value::from(0))
            .unwrap()
            .add_argument("RequestedCount", &glib::Value::from(0))
            .unwrap()
            .add_argument("SortCriteria", &glib::Value::from(""))
            .unwrap();

        let result = cds.call_action_future(&action).await;
        if result.is_err() {
            println!("Browse call failed: {:?}", result);
            return;
        }
        let iter = result.unwrap().unwrap().iterate();
        if iter.is_err() {
            println!("iter call failed: {:?}", iter);
            return;
        }
        let foo = iter.unwrap().unwrap();
        while foo.next() {
            if let Some(name) = foo.name() {
                if name != "Result" {
                    continue;
                }

                let parser = gupnp_av::DIDLLiteParser::new();
                let unrestricted_container: Rc<RefCell<Option<GString>>> =
                    Rc::new(RefCell::new(None));
                let container_closure = {
                    let target = unrestricted_container.clone();
                    move |_: &_, c: &DIDLLiteContainer| {
                        if !c.is_restricted() {
                            *std::cell::RefCell::<_>::borrow_mut(&target) = c.id();
                        }
                    }
                };

                let id = parser.connect_container_available(container_closure);
                let parse_result = parser.parse_didl(foo.value().unwrap().get::<&str>().unwrap());
                if let Err(err) = parse_result {
                    g_critical!(
                        "GUPnP-Upload",
                        "Failed to parse DIDL from media servers: {}",
                        err.message()
                    );
                }
                parser.disconnect(id);
                container = unrestricted_container
                    .borrow()
                    .as_ref()
                    .and_then(|v| Some(v.to_string()));
                break;
            }
        }
    }

    if container.is_none() {
        return;
    };

    println!("Found container for upload: {}", container.clone().unwrap());

    create_item(
        args.files.first().unwrap(),
        args.title.as_deref(),
        &cds,
        &container.unwrap(),
    )
    .await;
}

async fn create_item(
    path: &PathBuf,
    title: Option<&str>,
    proxy: &gupnp::ServiceProxy,
    container: &str,
) {
    let final_title = title.unwrap_or(path.file_name().unwrap_or_default().to_str().unwrap());
    let writer = DIDLLiteWriter::new(None);
    let item = writer.add_item().unwrap();
    item.set_parent_id(container);
    item.set_id("");
    item.set_restricted(false);
    item.set_title(final_title);

    let Some(class) = create_res_for_file(path, &item) else {
        g_critical!(
            "UPnP-Uploader",
            "Failed to guess UPnP class for file {:?}",
            path
        );
        return;
    };

    item.set_upnp_class(class.as_str());

    let Some(didl) = writer.string() else {
        g_critical!(
            "UPnP-Uploader",
            "Failed to serialize DIDL for file {:?}",
            path
        );
        return;
    };

    let action = ServiceProxyAction::new_plain("CreateObject")
        .add_argument("ContainerID", &glib::Value::from(container))
        .unwrap()
        .add_argument("Elements", &glib::Value::from(didl))
        .unwrap();

    let result = match proxy.call_action_future(&action).await {
        Ok(Some(result)) => result,
        Ok(None) => return,
        Err(err) => {
            g_critical!("UPnP-Upload", "Error calling action: {}", err.message());
            return;
        }
    };

    let iter = match result.iterate() {
        Ok(Some(iter)) => iter,
        Ok(None) => return,
        Err(err) => {
            g_critical!("UPnP-Upload", "Error calling action: {}", err.message());
            return;
        }
    };

    let mut uri: Option<String> = None;

    while iter.next() {
        if let Some(name) = iter.name() {
            if name != "Result" {
                continue;
            }
        }

        let parser = DIDLLiteParser::new();

        let import_uri: Rc<RefCell<Option<String>>> = Rc::new(RefCell::new(None));
        let target = import_uri.clone();
        let item_closure = move |_: &_, i: &DIDLLiteItem| {
            if (*std::cell::RefCell::<_>::borrow(&target)).is_some() {
                return;
            };
            let res = i.resources();
            let Some(resource) = res.first() else {
                return;
            };

            *std::cell::RefCell::<_>::borrow_mut(&target) =
                resource.import_uri().and_then(|x| Some(x.to_string()))
        };

        let id = parser.connect_item_available(item_closure);
        if let Err(parse_error) = parser.parse_didl(iter.value().unwrap().get::<&str>().unwrap()) {
            g_critical!(
                "GUPnP-Upload",
                "Failed to parse DIDL: {}",
                parse_error.message()
            );

            return;
        }
        parser.disconnect(id);

        uri = import_uri.borrow().as_ref().cloned();

        break;
    }

    if uri.is_none() {
        return;
    }
    let uri = uri;

    if !path.is_absolute() {
        g_critical!("UPnP-Upload", "Given path is not absolute: {:?}", path);
        return;
    }

    let file_size = std::fs::metadata(path).unwrap().len();

    let Some(file_path) = path.as_os_str().to_str() else {
        g_critical!("UPnP-Upload", "Cannot convert path {:?}", path);

        return;
    };

    let Some(context) = proxy.context() else {
        panic!("Must not happen");
    };

    context.host_path(file_path, file_path);

    let source_uri = format!(
        "http://{}:{}{}",
        context.host_ip().unwrap(),
        gupnp::prelude::ClientExt::port(&context),
        file_path
    );

    let action = ServiceProxyAction::new_plain("ImportResource")
        .add_argument("SourceURI", &glib::Value::from(source_uri))
        .unwrap()
        .add_argument("DestinationURI", &glib::Value::from(uri))
        .unwrap();

    let result = match proxy.call_action_future(&action).await {
        Ok(Some(result)) => result,
        Ok(None) => return,
        Err(err) => {
            g_critical!("UPnP-Upload", "Error calling action: {}", err.message());
            return;
        }
    };

    let iter = match result.iterate() {
        Ok(Some(iter)) => iter,
        Ok(None) => return,
        Err(err) => {
            g_critical!("UPnP-Upload", "Error calling action: {}", err.message());
            return;
        }
    };

    let bar = ProgressBar::new(file_size);
    if iter.next() && iter.name() == Some(GString::from("TransferID")) {
        let transfer_id = iter.value().unwrap();
        let transfer_action = ServiceProxyAction::new_plain("GetTransferProgress")
            .add_argument("TransferID", &transfer_id)
            .unwrap();

        'progress: loop {
            glib::timeout_future(Duration::from_secs(1)).await;
            let result = match proxy.call_action_future(&transfer_action).await {
                Ok(Some(result)) => result,
                Ok(None) => return,
                Err(err) => {
                    g_critical!("UPnP-Upload", "Error calling action: {}", err.message());
                    return;
                }
            };

            let iter = match result.iterate() {
                Ok(Some(iter)) => iter,
                Ok(None) => return,
                Err(err) => {
                    g_critical!("UPnP-Upload", "Error calling action: {}", err.message());
                    return;
                }
            };

            let mut length = None::<u64>;
            while iter.next() {
                match iter.name().unwrap().as_str() {
                    "TransferStatus" => {
                        let v = iter.value().unwrap();
                        let w = v.get::<&str>().unwrap();
                        match w {
                            "IN_PROGRESS" => {}
                            _ => break 'progress,
                        }
                    }
                    // For some reason those two parameters are not u64 in the service description but
                    // "the string type of data representing a numerical value that may exceed 32 bits in size"
                    "TransferLength" => {
                        let v = iter.value_as(glib::Type::U64).unwrap();
                        length = v.get::<u64>().ok();
                    }
                    _ => {}
                }
            }

            // If we are here, we are in transfer
            if length.is_some() {
                bar.inc(length.unwrap());
            } else {
                println!("Transfer is ongoing, progress unknown");
            }
        }
        bar.finish();
    }
}

fn create_res_for_file(path: &PathBuf, item: &DIDLLiteItem) -> Option<String> {
    let Some(res) = item.add_resource() else {
        return None;
    };

    res.set_uri("");
    let (content_type, _) = gio::content_type_guess(Some(path), &[]);
    let mime_type = gio::content_type_get_mime_type(&content_type.to_string().as_str()).unwrap();
    let upnp_class = guess_upnp_class_for_mime_type(mime_type.as_str());

    let info = ProtocolInfo::new();
    info.set_mime_type(mime_type.as_str());
    info.set_protocol("*");
    res.set_protocol_info(&info);

    upnp_class
}

fn guess_upnp_class_for_mime_type(mime: &str) -> Option<String> {
    if mime.starts_with("audio/") {
        Some(String::from("object.item.audioItem.musicTrack"))
    } else if mime.starts_with("video/") {
        Some(String::from("object.item.videoItem"))
    } else if mime.starts_with("image/") {
        Some(String::from("object.item.imageItem"))
    } else {
        None
    }
}

fn main() {
    let args = Args::parse();
    let c = glib::MainContext::default();
    let main_loop = MainLoop::new(Some(&c), false);

    let future = clone!(
        #[strong]
        main_loop,
        async move {
            upload(&args).await;
            main_loop.quit();
        }
    );

    c.spawn_local(future);

    main_loop.run();
}
