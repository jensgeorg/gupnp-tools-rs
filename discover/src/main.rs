use gio;
use glib;
use glib::MainLoop;

use gssdp;
use gssdp::prelude::*;

use clap::Parser;

const SIGTERM: i32 = 2;

/// Simple SSDP browser
#[derive(clap::Parser)]
struct Args {
    /// SSDP search target
    #[arg(short, long, default_value_t = String::from("ssdp:all"))]
    search_target: String,

    /// Network interface to use for device discovery
    #[arg(short, long)]
    interface: Option<String>,

    /// Time in seconds to run. 0 means never exit
    #[arg(short, long, default_value_t = 0)]
    timeout: u32,

    /// Time in seconds to wait before another discovery request
    #[arg(short, long, default_value_t = 0)]
    rescan_timeout: u32,
}

fn main() {
    let args = Args::parse();

    let client = gssdp::Client::new_full(
        args.interface.as_deref(),
        gio::InetAddress::NONE,
        0,
        gssdp::UDAVersion::Version10,
    )
    .unwrap();
    let browser = gssdp::ResourceBrowser::new(&client.unwrap(), &args.search_target);
    browser.connect_resource_available(|_, usn, locations| {
        println!("Resource available: {}", usn);
        for val in locations.iter() {
            println!("      location: {}", val);
        }
    });
    browser.connect_resource_unavailable(|_, usn| {
        println!("Resource unavailable: {}", usn);
    });
    browser.connect_resource_update(|_, usn, boot_id, next_boot_id| {
        println!(
            "Resource update: {} changes boot id from {} to {}",
            usn, boot_id, next_boot_id
        );
    });

    browser.set_active(true);

    let main_loop = MainLoop::new(None::<&glib::MainContext>, false);
    let loop_clone = main_loop.clone();
    let id = glib::source::unix_signal_add(SIGTERM, move || {
        loop_clone.quit();
        glib::ControlFlow::Continue
    });

    let mut timeout_id: Option<glib::SourceId> = None;
    if args.timeout > 0 {
        let loop_clone = main_loop.clone();
        timeout_id = Some(glib::source::timeout_add_seconds_local_once(
            args.timeout,
            move || {
                loop_clone.quit();
            },
        ));
    }

    let mut rescan_timeout_id: Option<glib::SourceId> = None;
    if args.rescan_timeout > 0 && (args.rescan_timeout < args.timeout || args.timeout == 0) {
        rescan_timeout_id = Some(glib::source::timeout_add_seconds_local(
            args.rescan_timeout,
            glib::clone!(
                #[weak]
                browser,
                #[upgrade_or]
                glib::ControlFlow::Break,
                move || {
                    browser.rescan();
                    glib::ControlFlow::Continue
                }
            ),
        ));
    }

    main_loop.run();
    id.remove();
    if timeout_id.is_some() {
        timeout_id.unwrap().remove();
    }

    if rescan_timeout_id.is_some() {
        rescan_timeout_id.unwrap().remove();
    }
}
