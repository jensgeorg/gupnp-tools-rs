use std::cell::{Cell, RefCell};
use std::sync::{Arc, Weak};

use glib::{clone, g_info, prelude::*};

use glib;
use glib::MainLoop;

use gupnp;
use gupnp::prelude::*;

#[derive(Debug)]
struct EventDumperData {
    proxies: RefCell<Vec<gupnp::ServiceProxy>>,
}

#[derive(Debug, Clone)]
struct EventDumper(Arc<EventDumperData>);

// To be able to access the App's fields directly
impl std::ops::Deref for EventDumper {
    type Target = EventDumperData;

    fn deref(&self) -> &EventDumperData {
        &self.0
    }
}

struct EventDumperWeak(Weak<EventDumperData>);
impl EventDumperWeak {
    // Try upgrading a weak reference to a strong one
    fn upgrade(&self) -> Option<EventDumper> {
        self.0.upgrade().map(EventDumper)
    }
}

impl EventDumper {
    pub fn new() -> Self {
        EventDumper(Arc::new(EventDumperData {
            proxies: RefCell::new(vec![]),
        }))
    }

    fn downgrade(&self) -> EventDumperWeak {
        EventDumperWeak(Arc::downgrade(&self.0))
    }

    fn context_available(&self, cm: &gupnp::ContextManager, context: &gupnp::Context) {
        g_info!(
            "event-dumper",
            "New Context: {}",
            context.host_ip().unwrap()
        );

        let control_point = gupnp::ControlPoint::new(context, "upnp:rootdevice");
        control_point.set_active(true);

        let clone_a = self.downgrade();
        control_point.connect_device_proxy_unavailable(move |_, device| {
            let app = clone_a.upgrade().unwrap();

            app.proxies.borrow_mut().retain(|x| x.udn() != device.udn());
        });

        let clone_b = self.downgrade();
        control_point.connect_device_proxy_available(move |cp, device| {
            clone_b
                .upgrade()
                .and_then(|ed| Some(ed.on_proxy_available(cp, device)));
        });

        cm.manage_control_point(&control_point);
    }

    pub fn run(&self) {
        if let Some(context_manager) = gupnp::ContextManager::create(0) {
            let app = self.downgrade();
            context_manager.connect_context_available(move |cm, ctx| {
                app.upgrade().unwrap().context_available(cm, ctx);
            });

            context_manager.connect_context_unavailable(|_, ctx| {
                g_info!("event-dumper", "Context gone: {}", ctx.host_ip().unwrap());
            });

            let main_loop = MainLoop::new(None::<&glib::MainContext>, false);
            main_loop.run();
        }
    }

    fn on_proxy_available(&self, _cp: &gupnp::ControlPoint, device: &gupnp::DeviceProxy) {
        let name = device.friendly_name().unwrap();
        let device_type = device.device_type().unwrap();
        let location = device.location().unwrap();

        g_info!(
            "event-dumper",
            "New device {}, type {} at {}",
            name,
            device_type,
            location
        );

        for service in device.list_services().iter() {
            let service_clone = service.clone().downcast::<gupnp::ServiceProxy>().unwrap();
            service_clone.set_subscribed(true);
            g_info!(
                "event-dumper",
                "Found new service proxy {}",
                service.id().unwrap()
            );
            let app = self.downgrade();
            service.introspect_async(None::<&gio::Cancellable>, move |res| match res {
                Err(err) => {
                    println!(
                        "Failed to introspect service proxy {}: {:?}",
                        service_clone.id().unwrap(),
                        err.message()
                    );
                }
                Ok(None) => {
                    panic!(
                        "No introspection available for {}, and no error, must not happen",
                        service_clone.id().unwrap()
                    );
                }
                Ok(Some(introspection)) => {
                    g_info!(
                        "event-dumper",
                        "Got introspection for {}",
                        service_clone.id().unwrap()
                    );
                    let variables = introspection.list_state_variables();
                    for variable in variables.iter() {
                        g_info!(
                            "event-dumper",
                            "Adding notify for {} (type {})",
                            variable.name(),
                            variable.type_().name()
                        );

                        let notification_callback = clone!(
                            #[weak(rename_to = service_capture)]
                            service_clone,
                            move |_: &gupnp::ServiceProxy, name: &str, value: &glib::Value| {
                                let ts =
                                    glib::DateTime::now_local().and_then(|v| v.format_iso8601());
                                println!(
                                    "{}|{}|{}|{}|{}",
                                    ts.unwrap(),
                                    service_capture.udn().unwrap(),
                                    service_capture.id().unwrap(),
                                    name,
                                    value.get::<String>().unwrap()
                                );
                            }
                        );

                        service_clone.add_notify(
                            variable.name(),
                            glib::types::Type::STRING,
                            notification_callback,
                        );
                        app.upgrade()
                            .and_then(|a| Some(a.proxies.borrow_mut().push(service_clone.clone())));
                    }
                }
            });
        }
    }
}

fn main() {
    let dumper = EventDumper::new();
    dumper.run();
}
